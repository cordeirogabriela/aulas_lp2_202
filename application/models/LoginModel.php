<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoginModel extends MY_Controller
{

    public function verifica()
    {
        if (sizeof($_POST) == 0) return 0;
        $email = $this->input->post('email');
        $senha = $this->input->post('email');

        // $res = $this->db->get_where('login', ['email' => $email, 'senha'=> $senha]);
        // $v = $res->result_array();
        $this->load->library('Login', '', 'acesso');
        $k = $this->acesso->verifica($email, $senha);

        if($k != 1){
            redirect('home');
        }else{
            return 1;
        }
       
    }
}
