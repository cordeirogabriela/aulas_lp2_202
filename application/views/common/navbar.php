<nav class="navbar navbar-expand-lg navbar-dark bg-primary">

    <div class="container">


        <a class="navbar-brand" href="#">Controle Financeiro</a>

        <ul class="nav navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url('home') ?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown btn-group">
                <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cadastro</a>
                <div class="dropdown-menu dropdown" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item" href="<?= base_url('usuario/cadastro')?>">Usuário</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/conta-bancaria')?>">Conta Bancária</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/parceiros')?>">Parceiros</a>
                </div>
            </li>
            <li class="nav-item dropdown btn-group">
                <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Lançamentos</a>
                <div class="dropdown-menu dropdown" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item"href="<?= base_url('usuario/contas-pagar')?>">Contas a pagar</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/contas-receber')?>">Contas a receber</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/fluxo-caixa')?>">Fluxo de caixa</a>
                </div>
            </li>
            
            <li class="nav-item dropdown btn-group">
                <a class="nav-link dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Relatórios</a>
                <div class="dropdown-menu dropdown" aria-labelledby="dropdownMenu1">
                    <a class="dropdown-item"href="<?= base_url('usuario/lancamento-periodo')?>">Lançamentos por período</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/resumo-mensal')?>">Resumo mensal</a>
                    <a class="dropdown-item"href="<?= base_url('usuario/resumo-anual')?>">Resumo anual</a>
                </div>
            </li>
        </ul>


    </div>

</nav>