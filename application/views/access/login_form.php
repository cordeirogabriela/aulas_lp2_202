<div style="height: 100vh">
  <div class="flex-center flex-column">
    <h3 class="mb-5">Controle Financeiro Pessoal</h3>

    <form class="text-center border border-light p-5" method="POST">
      <p class="h4 text-center mb-3">Entrar</p>
      <div class="form-outline mb-4">
        <input type="email" id="email" class="form-control" placeholder="E-mail" name="email" />
        <label class="form-label" for="form1Example1">Email</label>
      </div>

      <div class="form-outline mb-4">
        <input type="password" id="senha" class="form-control" placeholder="Senha" name="senha" />
        <label class="form-label" for="form1Example2">Senha</label>
      </div>

      <button type="submit" class="btn btn-primary btn-block my-4">Enviar</button>
      <p class="red-text"> <?= $error ? 'Dados incorretos.' : '' ?> </p>
    </form>


  </div>
</div>